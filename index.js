const express = require('express');
const app = express();
const cors = require('cors');
const mongoose = require('mongoose')


mongoose.connect('mongodb+srv://admin:admin@zuitt-bootcamp.ir0ca.mongodb.net/batch127_to-do?retryWrites=true&w=majority',
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	})

mongoose.connection.once('open',() => console.log('Now connect to MongoDB atlas'));


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended:true }));

app.listen(process.env.PORT || 4000, ()=> {
	console.log(`API is now Online on port ${ process.env.PORT || 4000}`)
})