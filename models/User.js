const mongoose = require('mongoose');
const courseSchema = new mongoose.courseSchema({
	firstName:{
		type: String,
		require:[true,"firstName is required"]
	},lastName:{
		type:String,
		required:[true,'lastName is required']
	},
	email:{
		type:String,
		required:[true,'email is required']
	},
	password:{
		type:String,
		required:[true,'password is required']
	}
	isAdmin:{
		type:String,
		default:
	},
	mobileNo:{
		type: String,
		required:[true,'mobileNo is required']
	},
	enrollments:[
	{
		CourseId:{
			type: String,
			default:
		},
		enrolledOn:{
			type:Date,
			default:new Date()
		},
		status:{
			type:String,
			 default:enrolled()
		}
	}
	]
});

module.exports = mongoose.model("User" , courseSchema);